//
//  main.m
//  demohtml
//
//  Created by tyndog on 2018/5/6.
//  Copyright © 2018年 Lzcarry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
