//
//  ViewController.m
//  demohtml
//
//  Created by tyndog on 2018/5/6.
//  Copyright © 2018年 Lzcarry. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>

@interface ViewController ()
@property (weak, nonatomic) WKWebView *wkview;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    WKWebView *wkview = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _wkview = wkview;
    [self.view addSubview:_wkview];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"demo2"];
    NSURL *fileURL = [NSURL fileURLWithPath:path];
    [self.wkview loadFileURL:fileURL allowingReadAccessToURL:fileURL];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
